################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../App/LoRaWAN/src/debug.c \
../App/LoRaWAN/src/hw_gpio.c \
../App/LoRaWAN/src/hw_rtc.c \
../App/LoRaWAN/src/hw_spi.c \
../App/LoRaWAN/src/main.c \
../App/LoRaWAN/src/vcom.c 

OBJS += \
./App/LoRaWAN/src/debug.o \
./App/LoRaWAN/src/hw_gpio.o \
./App/LoRaWAN/src/hw_rtc.o \
./App/LoRaWAN/src/hw_spi.o \
./App/LoRaWAN/src/main.o \
./App/LoRaWAN/src/vcom.o 

C_DEPS += \
./App/LoRaWAN/src/debug.d \
./App/LoRaWAN/src/hw_gpio.d \
./App/LoRaWAN/src/hw_rtc.d \
./App/LoRaWAN/src/hw_spi.d \
./App/LoRaWAN/src/main.d \
./App/LoRaWAN/src/vcom.d 


# Each subdirectory must supply rules for building sources it contributes
App/LoRaWAN/src/%.o App/LoRaWAN/src/%.su: ../App/LoRaWAN/src/%.c App/LoRaWAN/src/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m0plus -std=gnu11 -g3 -DUSE_HAL_DRIVER -DLOW_POWER_DISABLE -DDEBUG -DSTM32L072xx -DUSE_B_L072Z_LRWAN1 -DREGION_IN865 -c -I../Drivers/CMSIS/Device/ST/STM32L0xx/Include -I../Drivers/STM32L0xx_HAL_Driver -I../Drivers/CMSIS/Include -I../Drivers/STM32L0xx_HAL_Driver/Inc -I../Drivers/Middlewares/LoRaWAN/Mac/region -I../Drivers/Middlewares/LoRaWAN/Patterns/Basic -I../Drivers/Middlewares/LoRaWAN/Phy -I../Drivers/Middlewares/LoRaWAN/Utilities -I../Drivers/BSP/B-L072Z-LRWAN1 -I../Drivers/BSP/Components/sx1276 -I../App/Core/Inc -I../App/LoRaWAN/inc -I../App/Aquaponics -I../App/Startup -I../Drivers/Middlewares/LoRaWAN/Crypto -I../Drivers/Middlewares/LoRaWAN/Mac -I../Drivers/BSP/CMWX1ZZABZ-0xx -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

clean: clean-App-2f-LoRaWAN-2f-src

clean-App-2f-LoRaWAN-2f-src:
	-$(RM) ./App/LoRaWAN/src/debug.d ./App/LoRaWAN/src/debug.o ./App/LoRaWAN/src/debug.su ./App/LoRaWAN/src/hw_gpio.d ./App/LoRaWAN/src/hw_gpio.o ./App/LoRaWAN/src/hw_gpio.su ./App/LoRaWAN/src/hw_rtc.d ./App/LoRaWAN/src/hw_rtc.o ./App/LoRaWAN/src/hw_rtc.su ./App/LoRaWAN/src/hw_spi.d ./App/LoRaWAN/src/hw_spi.o ./App/LoRaWAN/src/hw_spi.su ./App/LoRaWAN/src/main.d ./App/LoRaWAN/src/main.o ./App/LoRaWAN/src/main.su ./App/LoRaWAN/src/vcom.d ./App/LoRaWAN/src/vcom.o ./App/LoRaWAN/src/vcom.su

.PHONY: clean-App-2f-LoRaWAN-2f-src

