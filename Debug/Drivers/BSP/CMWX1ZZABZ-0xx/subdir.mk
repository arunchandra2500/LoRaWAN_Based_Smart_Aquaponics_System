################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Drivers/BSP/CMWX1ZZABZ-0xx/mlm32l07x01.c 

OBJS += \
./Drivers/BSP/CMWX1ZZABZ-0xx/mlm32l07x01.o 

C_DEPS += \
./Drivers/BSP/CMWX1ZZABZ-0xx/mlm32l07x01.d 


# Each subdirectory must supply rules for building sources it contributes
Drivers/BSP/CMWX1ZZABZ-0xx/%.o Drivers/BSP/CMWX1ZZABZ-0xx/%.su: ../Drivers/BSP/CMWX1ZZABZ-0xx/%.c Drivers/BSP/CMWX1ZZABZ-0xx/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m0plus -std=gnu11 -g3 -DUSE_HAL_DRIVER -DLOW_POWER_DISABLE -DDEBUG -DSTM32L072xx -DUSE_B_L072Z_LRWAN1 -DREGION_IN865 -c -I../Drivers/CMSIS/Device/ST/STM32L0xx/Include -I../Drivers/STM32L0xx_HAL_Driver -I../Drivers/CMSIS/Include -I../Drivers/STM32L0xx_HAL_Driver/Inc -I../Drivers/Middlewares/LoRaWAN/Mac/region -I../Drivers/Middlewares/LoRaWAN/Patterns/Basic -I../Drivers/Middlewares/LoRaWAN/Phy -I../Drivers/Middlewares/LoRaWAN/Utilities -I../Drivers/BSP/B-L072Z-LRWAN1 -I../Drivers/BSP/Components/sx1276 -I../App/Core/Inc -I../App/LoRaWAN/inc -I../App/Aquaponics -I../App/Startup -I../Drivers/Middlewares/LoRaWAN/Crypto -I../Drivers/Middlewares/LoRaWAN/Mac -I../Drivers/BSP/CMWX1ZZABZ-0xx -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

clean: clean-Drivers-2f-BSP-2f-CMWX1ZZABZ-2d-0xx

clean-Drivers-2f-BSP-2f-CMWX1ZZABZ-2d-0xx:
	-$(RM) ./Drivers/BSP/CMWX1ZZABZ-0xx/mlm32l07x01.d ./Drivers/BSP/CMWX1ZZABZ-0xx/mlm32l07x01.o ./Drivers/BSP/CMWX1ZZABZ-0xx/mlm32l07x01.su

.PHONY: clean-Drivers-2f-BSP-2f-CMWX1ZZABZ-2d-0xx

