################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Drivers/Middlewares/LoRaWAN/Patterns/Basic/lora-test.c \
../Drivers/Middlewares/LoRaWAN/Patterns/Basic/lora.c 

OBJS += \
./Drivers/Middlewares/LoRaWAN/Patterns/Basic/lora-test.o \
./Drivers/Middlewares/LoRaWAN/Patterns/Basic/lora.o 

C_DEPS += \
./Drivers/Middlewares/LoRaWAN/Patterns/Basic/lora-test.d \
./Drivers/Middlewares/LoRaWAN/Patterns/Basic/lora.d 


# Each subdirectory must supply rules for building sources it contributes
Drivers/Middlewares/LoRaWAN/Patterns/Basic/%.o Drivers/Middlewares/LoRaWAN/Patterns/Basic/%.su: ../Drivers/Middlewares/LoRaWAN/Patterns/Basic/%.c Drivers/Middlewares/LoRaWAN/Patterns/Basic/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m0plus -std=gnu11 -g3 -DUSE_HAL_DRIVER -DLOW_POWER_DISABLE -DDEBUG -DSTM32L072xx -DUSE_B_L072Z_LRWAN1 -DREGION_IN865 -c -I../Drivers/CMSIS/Device/ST/STM32L0xx/Include -I../Drivers/STM32L0xx_HAL_Driver -I../Drivers/CMSIS/Include -I../Drivers/STM32L0xx_HAL_Driver/Inc -I../Drivers/Middlewares/LoRaWAN/Mac/region -I../Drivers/Middlewares/LoRaWAN/Patterns/Basic -I../Drivers/Middlewares/LoRaWAN/Phy -I../Drivers/Middlewares/LoRaWAN/Utilities -I../Drivers/BSP/B-L072Z-LRWAN1 -I../Drivers/BSP/Components/sx1276 -I../App/Core/Inc -I../App/LoRaWAN/inc -I../App/Aquaponics -I../App/Startup -I../Drivers/Middlewares/LoRaWAN/Crypto -I../Drivers/Middlewares/LoRaWAN/Mac -I../Drivers/BSP/CMWX1ZZABZ-0xx -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

clean: clean-Drivers-2f-Middlewares-2f-LoRaWAN-2f-Patterns-2f-Basic

clean-Drivers-2f-Middlewares-2f-LoRaWAN-2f-Patterns-2f-Basic:
	-$(RM) ./Drivers/Middlewares/LoRaWAN/Patterns/Basic/lora-test.d ./Drivers/Middlewares/LoRaWAN/Patterns/Basic/lora-test.o ./Drivers/Middlewares/LoRaWAN/Patterns/Basic/lora-test.su ./Drivers/Middlewares/LoRaWAN/Patterns/Basic/lora.d ./Drivers/Middlewares/LoRaWAN/Patterns/Basic/lora.o ./Drivers/Middlewares/LoRaWAN/Patterns/Basic/lora.su

.PHONY: clean-Drivers-2f-Middlewares-2f-LoRaWAN-2f-Patterns-2f-Basic

