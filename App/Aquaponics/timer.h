/*
 * timer.h
 *
 *  Created on: Feb 15, 2022
 *      Author: arun
 */

#ifndef AQUAPONICS_TIMER_H_
#define AQUAPONICS_TIMER_H_


#include <hw.h>

extern TIM_HandleTypeDef htim6;
void MX_TIM6_Init(void);
void HAL_TIM_Base_MspInit(TIM_HandleTypeDef* htim_base);
void HAL_TIM_Base_MspDeInit(TIM_HandleTypeDef* htim_base);



#endif /* AQUAPONICS_TIMER_H_ */
