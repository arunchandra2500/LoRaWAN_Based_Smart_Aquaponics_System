/*
 * aquaponics.h
 *
 *  Created on: 28-Jan-2022
 *      Author: arun
 */




typedef struct {

	uint8_t sensorOut;

	uint16_t pH;

	uint8_t sensorState;

	uint16_t Temperature;

	uint32_t totalWater;

	uint32_t DOsensor;

	uint16_t EC;

} aquaSensors_t;


void Set_Pin_Output (GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin);
void Set_Pin_Input (GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin);
uint8_t DS18B20_Start (void);
void DS18B20_Write (uint8_t data);
uint8_t DS18B20_Read (void);
uint16_t dataTemp ();
void aquaponicsInit();
uint8_t getWaterLevel();
uint8_t getWaterLevel_High();
void downlinkRectification(uint8_t data);
void aquaponicsGPIOinit() ;
void readSystemParameters(aquaSensors_t *sensor_data);
uint16_t pH_test();
uint32_t flowSensor();
uint8_t waterLevel();
void flowSensorInterruptEnable();
void waterPulseCount();
uint16_t dissolvedOxygen();
int16_t readDO(uint16_t voltage_mv, uint16_t temperature_c);
uint16_t electricalConductivity();



