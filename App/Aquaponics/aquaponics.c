/*
 * aquaponics.c
 *
 *  Created on: 28-Jan-2022
 *      Author: arun
 */

#ifndef AQUAPONICS_AQUAPONICS_C_
#define AQUAPONICS_AQUAPONICS_C_

#include <string.h>
#include <stdlib.h>
#include "hw.h"
#include "util_console.h"
#include "lora.h"
#include "timer.h"
#include "i2c.h"
#include "stm32l0xx_hal_i2c.h"
#include "aquaponics.h"

#define VREF 5000    //VREF (mv)
#define ADC_RES 4096 //ADC Resolution

//Single-point calibration Mode=0
//Two-point calibration Mode=1
#define TWO_POINT_CALIBRATION 0

//Single point calibration needs to be filled CAL1_V and CAL1_T
#define CAL1_V (1600) //mv
#define CAL1_T (25)   //℃
//Two-point calibration needs to be filled CAL2_V and CAL2_T
//CAL1 High temperature point, CAL2 Low temperature point
#define CAL2_V (1300) //mv
#define CAL2_T (15)   //℃


const uint16_t DO_Table[41] = {
    14460, 14220, 13820, 13440, 13090, 12740, 12420, 12110, 11810, 11530,
    11260, 11010, 10770, 10530, 10300, 10080, 9860, 9660, 9460, 9270,
    9080, 8900, 8730, 8570, 8410, 8250, 8110, 7960, 7820, 7690,
    7560, 7430, 7300, 7180, 7070, 6950, 6840, 6730, 6630, 6530, 6410};

//extern uint8_t data;
uint16_t adc_value;
uint8_t flag=0;
uint16_t liquid_level_low;
uint16_t liquid_level_high;
uint8_t flag1=0;


extern LoraFlagStatus AppProcessRequest;

extern int downlink_data;
//I2C_HandleTypeDef hi2c1; /*   handler for i2c   */

extern uint32_t startTime;
uint32_t start ;
uint32_t endTime = 0;
uint32_t end = 0;
uint32_t elapsedTime=0;
float LS = 0 ;
float TOTAL = 0;
volatile int flow_frequency=0;// Measures flow sensor pulses
float l_hour=0;// Calculated litres/hour
unsigned char flowsensor=2;// Sensor Input

uint16_t waterHour=0;
uint32_t water_ls = 0;
static uint32_t totalWater = 0;

unsigned long int avgValue;  //Store the average value of the sensor feedback
float b;
int buf[10],temp=0;
extern LoraFlagStatus EmergencyFlag;
//extern LoraFlagStatus AppProcessRequest;



//TIM_HandleTypeDef htim6;
uint8_t Rh_byte1, Rh_byte2, Temp_byte1, Temp_byte2;
uint16_t SUM, RH, TEMP;

uint16_t Temperature = 0;
uint8_t Presence = 0;

/**************************************************************DS18B20**********************************************************************************/

/* microsecond delay for the DS18B20 temperature sensor*/

void delay (uint16_t time)
{
	/* change your code here for the delay in microseconds */
	__HAL_TIM_SET_COUNTER(&htim6, 0);
	while ((__HAL_TIM_GET_COUNTER(&htim6))<time);
}

/* DS18B20 temperature sensor pin initialization */

void Set_Pin_Output (GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin)
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};
	GPIO_InitStruct.Pin = GPIO_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOx, &GPIO_InitStruct);
}

void Set_Pin_Input (GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin)
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};
	GPIO_InitStruct.Pin = GPIO_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	HAL_GPIO_Init(GPIOx, &GPIO_InitStruct);
}

#define DS18B20_PORT GPIOB
#define DS18B20_PIN GPIO_PIN_7

/*DS18B20 temperature sensor start */

uint8_t DS18B20_Start (void)
{
	uint8_t Response = 0;
	Set_Pin_Output(DS18B20_PORT, DS18B20_PIN);   // set the pin as output
	HAL_GPIO_WritePin (DS18B20_PORT, DS18B20_PIN, 0);  // pull the pin low
	delay (480);   // delay according to datasheet

	Set_Pin_Input(DS18B20_PORT, DS18B20_PIN);    // set the pin as input
	delay (80);    // delay according to datasheet

	if (!(HAL_GPIO_ReadPin (DS18B20_PORT, DS18B20_PIN))) Response = 1;    // if the pin is low i.e the presence pulse is detected
	else Response = -1;

	delay (400); // 480 us delay totally.

	return Response;
}

/*DS18B20 temperature sensor write*/

void DS18B20_Write (uint8_t data)
{
	Set_Pin_Output(DS18B20_PORT, DS18B20_PIN);  // set as output

	for (int i=0; i<8; i++)
	{

		if ((data & (1<<i))!=0)  // if the bit is high
		{
			// write 1

			Set_Pin_Output(DS18B20_PORT, DS18B20_PIN);  // set as output
			HAL_GPIO_WritePin (DS18B20_PORT, DS18B20_PIN, 0);  // pull the pin LOW
			delay (1);  // wait for 1 us

			Set_Pin_Input(DS18B20_PORT, DS18B20_PIN);  // set as input
			delay (50);  // wait for 60 us
		}

		else  // if the bit is low
		{
			// write 0

			Set_Pin_Output(DS18B20_PORT, DS18B20_PIN);
			HAL_GPIO_WritePin (DS18B20_PORT, DS18B20_PIN, 0);  // pull the pin LOW
			delay (50);  // wait for 60 us

			Set_Pin_Input(DS18B20_PORT, DS18B20_PIN);
		}
	}
}

/*DS18B20 temperature sensor read */

uint8_t DS18B20_Read (void)
{
	uint8_t value=0;

	Set_Pin_Input(DS18B20_PORT, DS18B20_PIN);

	for (int i=0;i<8;i++)
	{
		Set_Pin_Output(DS18B20_PORT, DS18B20_PIN);   // set as output

		HAL_GPIO_WritePin (DS18B20_PORT, DS18B20_PIN, 0);  // pull the data pin LOW
		delay (1);  // wait for > 1us

		Set_Pin_Input(DS18B20_PORT, DS18B20_PIN);  // set as input
		if (HAL_GPIO_ReadPin (DS18B20_PORT, DS18B20_PIN))  // if the pin is HIGH
		{
			value |= 1<<i;  // read = 1
		}
		delay (50);  // wait for 60 us
	}
	return value;
}
/*  getDS18B20_Temp  */
/**
 * Created on: Feb 15, 2022
 * Last Edited: Feb 15, 2022
 * Author: Arun
 *
 * @brief  read temperature
 * @param  none
 * @retval temperature
 *
 **/

uint16_t dataTemp (){

	 Presence = DS18B20_Start ();
		  	  HAL_Delay (1);
		  	  DS18B20_Write (0xCC);  // skip ROM
		  	  DS18B20_Write (0x44);  // convert t
		  	  HAL_Delay (800);

		  	  Presence = DS18B20_Start ();
		        HAL_Delay(1);
		        DS18B20_Write (0xCC);  // skip ROM
		        DS18B20_Write (0xBE);  // Read Scratch-pad

		        Temp_byte1 = DS18B20_Read();
		  	  Temp_byte2 = DS18B20_Read();
		  	  TEMP = (Temp_byte2<<8)|Temp_byte1;
		  	  Temperature = ((float)TEMP/16)*100;
#ifdef ENABLE_PRINT
		  	PRINTF("TEMP:- %d \r\n ", Temperature);
		  	PRINTF("TEMPr:- %d \r\n ", TEMP);
#endif
             return Temperature;
}

/***************************************************************************************************************************************************/

/* Status of lower Water level sensor */

uint8_t getWaterLevel_Low(){

	uint8_t liquid_level_low;

	liquid_level_low= HAL_GPIO_ReadPin (GPIOB, GPIO_PIN_2);
#ifdef ENABLE_PRINT
	PRINTF("liquid_level_low=");
	PRINTF("%d\r\n",liquid_level_low);
	HAL_Delay(400);
#endif
	return liquid_level_low;
}

/* Status of higher Water level sensor */

uint8_t getWaterLevel_High(){

	uint8_t liquid_level_high;
	liquid_level_high= HAL_GPIO_ReadPin (GPIOB, GPIO_PIN_15);
#ifdef ENABLE_PRINT
	PRINTF("liquid_level_high=");
	PRINTF("%d\r\n",liquid_level_high);
	HAL_Delay(400);
#endif
	return liquid_level_high;
}

/*  getwaterLevel  */
/**
 * Created on: Feb 2, 2022
 * Last Edited: Feb 2, 2022
 * Author: Arun
 *
 * @brief  read water level sensor status
 * @param  none
 * @retval water level
 *
 **/

uint8_t waterLevel()
{
	extern uint8_t sensorValue;
	if(getWaterLevel_Low()&&getWaterLevel_High()){
		sensorValue=2;
	}
	else if(getWaterLevel_Low()&&!getWaterLevel_High()){
		sensorValue=1;
	}
	else if(!getWaterLevel_Low()&&!getWaterLevel_High()){
		sensorValue=0;
	}
	return sensorValue;
}

/*  downlinkRectification  */
/**
 * Created on: Feb 2, 2022
 * Last Edited: Feb 2, 2022
 * Author: Arun
 *
 * @brief check the downlink and Rectify the issue
 * @param none
 * @retval Issue identification and correction
 *
 **/

void downlinkRectification(uint8_t data){

	if(data==2){

		while(waterLevel()==2){
#ifdef ENABLE_PRINT
		PRINTF("Water Level is normal!\r\n");
		PRINTF("Motor OFF\r\n");
#endif
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9, GPIO_PIN_RESET);

		}
#ifdef ENABLE_PRINT
			PRINTF("Water Level is Lower than the upper threshold!\r\n");
			PRINTF("Motor ON\r\n");
#endif
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9, GPIO_PIN_SET);

	}
	else if(data==1){

	  while(waterLevel()==1){
#ifdef ENABLE_PRINT
		  PRINTF("Water Level is Lower than the upper threshold!\r\n");
		  PRINTF("Motor ON\r\n");
#endif
		  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9, GPIO_PIN_SET);
	}
#ifdef ENABLE_PRINT
	  PRINTF("Water Level is normal!\r\n");
	  PRINTF("Motor OFF\r\n");
#endif
	  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9, GPIO_PIN_RESET);

}
	else if(data==0)
	{
		while((!getWaterLevel_Low()&& !getWaterLevel_High())||(getWaterLevel_Low()&& !getWaterLevel_High())){
#ifdef ENABLE_PRINT
			PRINTF("Water Level is Lower than the Lower threshold!\r\n");
			PRINTF("Motor ON\r\n");
#endif
			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9, GPIO_PIN_SET);

		}
#ifdef ENABLE_PRINT
		PRINTF("Water Level is normal!\r\n");
		PRINTF("Motor OFF\r\n");
#endif
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9, GPIO_PIN_RESET);
		AppProcessRequest = LORA_SET;

	}
	else if (data==15){

		while((getWaterLevel_Low()|| getWaterLevel_High())&&(pH_test()<600||pH_test()>800)&& (downlink_data!=5) && dissolvedOxygen()<=227 ){

			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_10, GPIO_PIN_SET);
		}
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_10, GPIO_PIN_RESET);

		while(!getWaterLevel_Low()|| !getWaterLevel_High()){

			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9, GPIO_PIN_SET);
		}
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9, GPIO_PIN_RESET);
	}
	else if (data==16){

		while(!getWaterLevel_Low()|| !getWaterLevel_High()){

			HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9, GPIO_PIN_SET);
				}
		HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9, GPIO_PIN_RESET);
	}

}

/*  aquaponicsGPIO_Init  */
/**
 * Created on: Feb 2, 2022
 * Last Edited: Feb 2, 2022
 * Author: Arun
 *
 * @brief initialize all the gpio
 * @param none
 * @retval none
 *
 **/
void aquaponicsGPIOinit() {

	GPIO_InitTypeDef initStruct = { 0 };

	initStruct.Pull = GPIO_NOPULL;
	initStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	initStruct.Mode = GPIO_MODE_OUTPUT_PP;
	HW_GPIO_Init(GPIOA, GPIO_PIN_9, &initStruct);
	HW_GPIO_Init(GPIOA, GPIO_PIN_10, &initStruct);

	initStruct.Mode = GPIO_MODE_INPUT;
	initStruct.Pull = GPIO_NOPULL;
	HW_GPIO_Init(GPIOB, GPIO_PIN_15, &initStruct);
	HW_GPIO_Init(GPIOB, GPIO_PIN_2, &initStruct);

}
/*  readpH  */
/**
 * Created on: Feb 2, 2022
 * Last Edited: Feb 2, 2022
 * Author: Arun
 *
 * @brief read data from available API
 * @param sensor data pointer that contains the variables to be read from the sensor
 * @retval pH
 *
 **/
uint16_t pH_test()
{
	for(int i=0;i<10;i++)       //Get 10 sample value from the sensor for smooth the value
	  {
		buf[i] = HW_AdcReadChannel(ADC_CHANNEL_0);
		//PRINTF("%ld\n",HW_AdcReadChannel(ADC_CHANNEL_0));
		HAL_Delay(400);
		//HW_AdcReadChannel(ADC_CHANNEL_4);
	    HAL_Delay(10);
	  }
	  for(int i=0;i<9;i++)        //sort the analog from small to large
	  {
	    for(int j=i+1;j<10;j++)
	    {
	      if(buf[i]>buf[j])
	      {
	        temp=buf[i];
	        buf[i]=buf[j];
	        buf[j]=temp;
	      }
	    }
	  }
	  avgValue=0;
	  for(int i=2;i<8;i++)                      //take the average value of 6 center sample
	    avgValue+=buf[i];
	  float phValue=(float)avgValue*5.0/4096/6; //convert the analog into millivolt
	  uint16_t pHlevel;
	  pHlevel=3.2*phValue*100;                      //convert the millivolt into pH value
#ifdef ENABLE_PRINT
	  PRINTF("pH:");
	  PRINTF("%d\r\n",pHlevel);
	  PRINTF(" ");
#endif

return pHlevel;
}

/*  InterruptEnable  */
/**
 * Created on: Feb 19, 2022
 * Last Edited: Feb 19, 2022
 * Author: Arun
 *
 * @brief initiaze interrupt pins
 * @param none
 * @retval none
 *
 **/
void flowSensorInterruptEnable() {
	GPIO_InitTypeDef initStruct = { 0 };

	initStruct.Mode = GPIO_MODE_IT_FALLING;
	initStruct.Pull = GPIO_PULLUP;
	initStruct.Speed = GPIO_SPEED_HIGH;

	HW_GPIO_Init(GPIOA, GPIO_PIN_8, &initStruct);
	HW_GPIO_SetIrq(GPIOA, GPIO_PIN_8, 1, waterPulseCount);
}

/*  water flow pulse count  */
/**
 * Created on: Feb 19, 2022
 * Last Edited: Feb 19, 2022
 * Author: Arun
 *
 * @brief callback function interrupt on PB9 pin
 * @note this pin PB9 is connected to a Flow sensor output
 * @param none
 * @retval none
 *
 **/
void waterPulseCount(){

	flow_frequency++;
	PRINTF("startTime : %ld \r\n",flow_frequency);
	flowSensor();

}

/*  getflowRate
 * Created on: Feb 19, 2022
 * Last Edited: Feb 19, 2022
 * Author: Arun
 *
 * @brief get flow rate and total water at transmission interval
 * @note none
 * @param none
 * @retval total water flows through the pipe line at specified interval
 *
 **/
uint32_t flowSensor(){


	//startTime = TimerGetCurrentTime();
#ifdef ENABLE_PRINT
	PRINTF("startTime : %ld \r\n",startTime);
#endif
	if((TimerGetCurrentTime()-startTime)>=1000)
	{
		startTime=TimerGetCurrentTime();
#ifdef ENABLE_PRINT
		PRINTF("elapsedTime : %ld \r\n",TimerGetCurrentTime());
#endif
		l_hour=(float)(flow_frequency/7.5);// (Pulse frequency x 60 min) / 7.5Q = flowrate in L/hour
		//PRINTF("l_hour: %.2f \r\n",l_hour);
		//waterHour= (uint32_t)(l_hour*100);
		LS = (float)(l_hour/60);
		//water_ls = (uint32_t)(LS*100);
		TOTAL = (float)(TOTAL+LS);
		totalWater = (float)(TOTAL)*100;
#ifdef ENABLE_PRINT
		PRINTF("water: %ld \r\n",waterHour);
		PRINTF("LS: %ld \r\n",water_ls);
#endif
		PRINTF("Total water: %ld \r\n",totalWater);
		flow_frequency=0;
}
	return totalWater;
}

uint16_t electricalConductivity(){

	float temperaturec=0;
	uint16_t AnalogAverage =0;
	float averageVoltage=0;
	float TempCoefficient=0;
	float CoefficientVolatge=0;
	typedef int Bool;
	Bool enterCalibrationFlag = 0;
	float ECvalue=0;
	uint16_t ECout=0;
	float ECvalueRaw=0;
	float compensationFactor;


	 temperaturec= (float)dataTemp()/100;
	 for(int i=0;i<10;i++){
	AnalogAverage += HW_AdcReadChannel(ADC_CHANNEL_4);
	 }
	 AnalogAverage=AnalogAverage/10;
	averageVoltage= (float) AnalogAverage*VREF/4096.0;
	if(temperaturec == 4095)
			        {
			            temperaturec = 25.0;      //when no temperature sensor ,temperature should be 25^C default
			            //PRINTF ("temperature:- %.2f ", temperaturec);
			        }else{
			        	//PRINTF ("temperature:- %.2f ", temperaturec);    //current temperature

			        }
			        TempCoefficient=(float)(1.0+0.0185*(temperaturec-25.0));    //temperature compensation formula: fFinalResult(25^C) = fFinalResult(current)/(1.0+0.0185*(fTP-25.0));
			       //PRINTF ("TempCoefficient:- %d ", TempCoefficient);
			        CoefficientVolatge=(float)(averageVoltage/TempCoefficient);
			       //PRINTF ("CoefficientVolatge:- %0.2f ", CoefficientVolatge);
				   if(CoefficientVolatge<22)PRINTF ("No solution!\n");   //25^C 1413us/cm<-->about 216mv  if the voltage(compensate)<150,that is <1ms/cm,out of the range
			       else if(CoefficientVolatge>3300)PRINTF ("Out of the range!\n");  //>20ms/cm,out of the range
			       else{
			        if(CoefficientVolatge<=448)ECvalue=(float)(4.2*CoefficientVolatge-64.32);   //1ms/cm<EC<=3ms/cm
			        else if(CoefficientVolatge<=1457)ECvalue=(float)(3.58*CoefficientVolatge-127);  //3ms/cm<EC<=10ms/cm
			        else ECvalue=(float)(3.8*CoefficientVolatge+2278);                           //10ms/cm<EC<20ms/cm
			        ECvalueRaw = (float)(ECvalue/1000.0);
			        ECout = (uint16_t)ECvalue;    //after compensation,convert us/cm to ms/cm

			       }
			  	//}
				   PRINTF ("ECvalue:- %d ", ECout);
				   PRINTF("ms/cm");
		 return ECout;

}


uint16_t dissolvedOxygen(){

	uint16_t Temperaturet=0;
	uint16_t ADC_Raw=0;
	uint16_t ADC_Voltage=0;
	uint16_t dissolveOxygen=0;

	Temperaturet = (uint16_t)dataTemp()/100;
	ADC_Raw = HW_AdcReadChannel(ADC_CHANNEL_5);
	ADC_Voltage = (uint16_t)(VREF) * ADC_Raw / ADC_RES;
#ifdef ENABLE_PRINT
	PRINTF("Temperaturet: %d", Temperaturet);
	PRINTF("ADC RAW: %d",ADC_Raw);
	PRINTF("ADC Voltage: %d",ADC_Voltage );
	dissolveOxygen = readDO(ADC_Voltage, Temperaturet);
	PRINTF("DO: %d  \n ",dissolveOxygen);
#endif
	return readDO(ADC_Voltage, Temperaturet);
}

int16_t readDO(uint16_t voltage_mv, uint16_t temperature_c)
{
#if TWO_POINT_CALIBRATION == 0
  uint16_t V_saturation = (uint32_t)CAL1_V + (uint32_t)35 * temperature_c - (uint32_t)CAL1_T * 35;
  return (voltage_mv * DO_Table[temperature_c] / V_saturation);
#else
  uint16_t V_saturation = (int16_t)((int8_t)temperature_c - CAL2_T) * ((uint16_t)CAL1_V - CAL2_V) / ((uint8_t)CAL1_T - CAL2_T) + CAL2_V;
  return (voltage_mv * DO_Table[temperature_c] / V_saturation);
#endif
}
/*  aquaponicsInit */
/**
 * Created on: Feb 2, 2022
 * Last Edited: Feb 15, 2022
 * Author: Arun
 *
 * @brief initialize  aquaponics system
 * @param none
 * @retval none
 *
 **/
void aquaponicsInit() {
	MX_I2C1_Init();

	MX_TIM6_Init();

	aquaponicsGPIOinit();

	flowSensorInterruptEnable();
}
/*  readSystemParameters*/
/**
 * Created on: Feb 2, 2022
 * Last Edited: Feb 15, 2022
 * Author: Arun
 *
 * @read sensor outputs
 * @param none
 * @retval none
 *
 **/
void readSystemParameters(aquaSensors_t *sensor_data) {

	sensor_data->sensorOut = waterLevel();

	sensor_data->pH = pH_test();

	sensor_data->Temperature = dataTemp ();

	sensor_data-> totalWater= totalWater;

	sensor_data-> DOsensor= dissolvedOxygen();

	sensor_data-> EC= electricalConductivity();

	totalWater=0;

}
#endif /* AQUAPONICS_AQUAPONICS_C_ */
