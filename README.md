# LoRaWAN Smart Balcony Aquaponics System


An aquaponic is a system that combines aquaculture with hydroponic to form a bio-integrated structure that aims at achieving a symbiotic environment for both aqualife and crops growth. Fish excretions in fish tanks contains ammonia which is poisonous and is considered very dangerous on fish survival. However, ammonia can be converted by the action of the nitrifying bacteria into Nitrites and then to Nitrates which can be used as nutrients for the plants, hence the symbiotic relationship between the two. The aim of this project is to monitor and control aquaponics by using Internet of Things (IoT) technology by recording various parameters in real time settings. Our proposed system consists of sensors that measure pH, temperature, Oxygen and EC levels. In turn, these sensors are connected to a remote database over the internet to enable users to get the values of these sensors. We used STM32 Based microcontriller to acquire sensors data and deliver them to a web server. We are using LoRaWAN technology for the communication of the system. The low power consumption of the system has resulted in the possibility of powering it by solar power. A normal pH level of 6.5 to 8 were recorded, the temperature was between (23-25.57) C° and Dissolved oxygen was should be greater than 5 ppm. These results are considered acceptable for a healthy growth of both aqualife and hydroplants.

LoRaWAN is a Low Power, Wide Area (LPWA) networking protocol designed to wirelessly connect battery operated `things' to the internet in regional, national or global networks, and targets key Internet of Things (IoT) requirements such as bi-directional communication, end-to-end security, mobility and localization services.

This system used to monitor the total consumption of water also. The system is designed to work fully automatic. Here our system consists of 4 parts, Data Acquisition Unit, Data Transmission Unit, Decision Making Unit and System Rectification Unit. The first part of the data acquisition consists of a set of sensors that collect parameters such as temperature, pH, DO, EC and water level. The second unit, the data transmission, contains the controller unit, which collects data from the data acquisition unit and sends it to the server. The third part is the decision making unit, which is basically the server part. The server compares the incoming data with the threshold value, if the value is greater than or less than the threshold, then sends it to a downlink controller and the last part is the system rectification unit, this unit depends on the incoming downlink data.


## Getting Started


- Make sure that you have a STM32 based Board.

- Install LoRaWAN software expansion code from [here](https://www.st.com/en/embedded-software/i-cube-lrwan.html) . Copy the code to ~/STM32CubeIDE/workspace/

- Select board : B-L072Z-LRWAN1 Development Board

## Prerequisites

STM32CubeIDE 1.1.0[Tested]

## License

This project is licensed under the MIT License - see the [LICENSE.md](https://gitlab.com/arunchandra2500/water-meter/-/blob/master/LICENCE.md) file for details

## Acknowledgments

- LoRaWAN software expansion code of [Semtech](https://www.st.com/en/embedded-software/i-cube-lrwan.html) and Stackforce
